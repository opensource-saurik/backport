#!/bin/bash

set -e

if [[ $# -eq 0 ]]; then
    flags=(--dirty="+")
else
    flags=("$@")
fi

monotonic=$(git log -1 --pretty=format:%ct)
version=$(git describe --tags --match="v*" "${flags[@]}" | sed -e 's@-\([^-]*\)-\([^-]*\)$@+\1.\2@;s@^v@@;s@%@~@g')

exec sed -e "
    s/\${monotonic}/${monotonic}/g;
    s/\${version}/${version}/g;
" AndroidManifest.xml.in >AndroidManifest.xml
